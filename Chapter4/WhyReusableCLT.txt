Being able to perform complex tasks with just a one-liner is what makes the command line powerful.
All the commands and pipelines that basically fit on one line can be called as one liners.

Some tasks you perform only once, and some you perform more often. Some tasks are very specific and 
others can be generalized. If you foresee or notice that you need to repeat a certain one-liner on a 
regular basis, it’s worthwhile to turn this into a command-line tool of its own. Command-line tools have
the best of both worlds: they can be used from the command line, accept parameters, and only have to be
created once.

First, we explain how to turn one-liners into reusable command-line tools. By adding parameters to our
commands, we can add the same flexibility that a programminglanguage offers. Subsequently, we demonstrate 
how to create reusable command-line tools from code you’ve written in a programming language. By following the Unix
philosophy, your code can be combined with other command-line tools, which maybe written in an entirely different language.



